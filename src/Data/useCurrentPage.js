import { useLocation } from "react-router-dom";


export const useCurrentPage = () => {
    const location = useLocation()

    let page = '';
    if (location?.pathname === '/') {
        page = 'Produits';
    } else if (location?.pathname === '/orders') {
        page = 'Commandes'
    } else {
        page = 'Clients'
    }
    return page;
}