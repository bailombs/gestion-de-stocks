import React from 'react';

const Card = ({ product }) => {
    return (
        <aside className='products__card'>

            <img src={product.picture} alt={product.title} className='products__card--image' />

            <div className='products__card--text'>
                <h3 className='title'>{product.title}</h3>
                <h5 className='price'>{`€ ${product.price}`}</h5>
            </div>
        </aside>
    );
};

export default Card;