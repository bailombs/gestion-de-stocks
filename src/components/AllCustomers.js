import { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomersQuery } from '../redux/features/customersApi';
import CardCustomer from './CardCustomer';


const AllCustomers = () => {
    const searchContent = useSelector((state) => state?.customers?.searchContent)
    const dispatch = useDispatch()
    const { data, error, isLoading, isSuccess, isFetching } = useCustomersQuery()

    useEffect(() => {
        // dispatch(getCustomers())
    }, [dispatch])


    const customers = data
    const CUSTOMERS = useMemo(() => customers, [customers])

    return (
        <section className='customers'>
            {
                CUSTOMERS
                    ?.filter((client) => {
                        return client?.raisonSocial?.toLowerCase().includes(searchContent.toLowerCase())
                    })
                    ?.map((client, index) => (
                        <CardCustomer key={`${client?.id}-${index}`} client={client} />
                    ))
            }

        </section>
    );
};

export default AllCustomers;