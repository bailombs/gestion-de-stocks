import React from 'react';
import { useCurrentPage } from '../Data/useCurrentPage';
const Header = () => {

    return (
        <header className='header'>
            <p className='header__text'>{useCurrentPage()}</p>
            <h1 className='header__logo'>Boukouty</h1>
        </header>
    );
};

export default Header;