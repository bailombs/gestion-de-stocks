import React, { useState } from 'react';
import { AiFillDelete, AiFillEdit } from 'react-icons/ai';
import { IoIosCheckmarkCircle } from 'react-icons/io';
import { useDeleteCustomerMutation, useUpdateCustomerMutation } from '../redux/features/customersApi';


const CardCustomer = ({ client }) => {
    const [edit, setEdit] = useState(false)
    const [inputSocial, setInputSocial] = useState('')
    const [inputAdress, setInputAddress] = useState('')
    const [inputEmail, setInputEmail] = useState('')
    const [inputTel, setInputTel] = useState('')
    const [inputDescription, setInputDescription] = useState('')


    const [deleteCustomer] = useDeleteCustomerMutation()
    const [updateCustomer] = useUpdateCustomerMutation()



    let isEditing = edit === false

    const handleDelete = async () => {
        await deleteCustomer(client?.id)
    }

    const handleEdit = async () => {
        try {
            await updateCustomer({
                id: client.id,
                "adresseMail": inputEmail ? inputEmail : client?.adresseMail,
                "adressePostal": inputAdress ? inputAdress : client?.adressePostal,
                "description": inputDescription ? inputDescription : client?.description,
                "raisonSocial": inputSocial ? inputSocial : client?.raisonSocial,
                "telephone": inputTel ? inputTel : client?.telephone
            })

        } catch (error) {
            console.log(error);
        }
    }

    return (
        <aside className='customers__card'>
            {
                isEditing ? (
                    <>
                        <div className='customers__card--wrapper'>
                            <h1>{client?.raisonSocial}</h1>
                            <p>{client?.adressePostal}</p>
                        </div>
                        <div className='customers__card--wrapper'>
                            <p>{client?.adresseMail}</p>
                            <p>{client?.telephone}</p>
                        </div>
                        <div className='customers__card--wrapper'>
                            <p>{client?.description}</p>
                        </div>
                    </>

                ) : (
                    <>
                        <div className='customers__card--wrapper'>
                            <input type="text"
                                defaultValue={client?.raisonSocial}
                                onChange={(e) => { setInputSocial(e.target.value) }}
                            />
                            <input type="text"
                                defaultValue={client?.adressePostal}
                                onChange={(e) => { setInputAddress(e.target.value) }}
                            />
                        </div>
                        <div className='customers__card--wrapper'>
                            <input type="text"
                                defaultValue={client?.adresseMail}
                                onChange={(e) => { setInputEmail(e.target.value) }}
                            />
                            <input type="text"
                                defaultValue={client?.telephone}
                                onChange={(e) => { setInputTel(e.target.value) }}
                            />
                        </div>
                        <div className='customers__card--wrapper'>
                            <textarea autoFocus
                                cols="num"
                                rows="num"
                                defaultValue={client?.description}
                                onChange={(e) => { setInputDescription(e.target.value) }}
                            >
                            </textarea>
                        </div>
                    </>
                )
            }

            <div className='customers__card--wrapper'>
                {isEditing ? (
                    <p>
                        <AiFillEdit className='edit-icon' onClick={() => { setEdit(true) }} />
                    </p>
                ) : (
                    <p>
                        <IoIosCheckmarkCircle className='valid-icon' onClick={() => { setEdit(false); handleEdit() }} />

                    </p>
                )}
                <p><AiFillDelete
                    className='delete-icon'
                    onClick={() => {
                        if (window.confirm("Voulez vraiment supprimer")) {
                            handleDelete()
                        }
                    }}
                /></p>
            </div>
        </aside>
    );
};

export default CardCustomer;