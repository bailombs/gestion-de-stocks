import React, { useEffect, useState } from 'react';
import { FaUserCircle } from 'react-icons/fa';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useAddNewCustomerMutation } from '../redux/features/customersApi';
import { getOpen } from '../redux/features/customersSlice';

const Modal = () => {
    const dispatch = useDispatch()
    const [social, setSocial] = useState('')
    const [address, setAdress] = useState('')
    const [email, setEmail] = useState('')
    const [tel, setTel] = useState('')
    const [description, setDescritption] = useState('')
    const [errorMsg, setErrorMsg] = useState('')
    let formatMailRegex = /^[a-z0-9.-_]+[@]{1}[a-z.-_]+[.]{1}[a-z]{2,8}$/
    const navigate = useNavigate()
    const [addNewCustomer] = useAddNewCustomerMutation()
    useEffect(() => {
        setErrorMsg('')
    }, [])

    //handleSave
    const handleSave = async (e) => {
        e.preventDefault();
        const data = {
            adresseMail: email,
            adressePostal: address,
            description: description,
            raisonSocial: social,
            telephone: tel
        }
        // if (social.trim() === "" || social.trim() < 3) {
        //     setErrorMsg("veuillez entrer le de l'entreprise ou de la personne")
        // }

        dispatch(getOpen(false))
        addNewCustomer(data)
        setSocial('')
        setAdress('')
        setEmail('')
        setTel('')
        setDescritption('')
    }

    return (
        <section className='modal'>
            <div className='modal__content'>

                <FaUserCircle />
                <h1 className='modal__content--title'>Ajout d'un client</h1>
                <form className='form' onSubmit={(e) => { handleSave(e) }}>
                    <div className='form__inptwrapper'>
                        <label htmlFor='raison-social'>Raison social *</label>
                        <input type="text" id='raison-social' onChange={(e) => { setSocial(e.target.value) }} />
                    </div>
                    <div className='form__inptwrapper'>
                        <label htmlFor='address'>Addresse</label>
                        <input type="text" id='address' onChange={(e) => { setAdress(e.target.value) }} />
                    </div>
                    <div className='form__inptwrapper'>
                        <label htmlFor='email'>Email</label>
                        <input type="email" id='email' onChange={(e) => { setEmail(e.target.value) }} />
                    </div>
                    <div className='form__inptwrapper'>
                        <label htmlFor='tel'>Tel *</label>
                        <input type="tel" id='tel' onChange={(e) => { setTel(e.target.value) }} />
                    </div>
                    <div className='form__inptwrapper'>
                        <label htmlFor='description'>Description</label>
                        <textarea id="description" cols="num" rows="num" onChange={(e) => { setDescritption(e.target.value) }}></textarea>
                    </div>
                    {
                        <p className='form__error'></p>
                    }
                    <div className='form__wrapper-close'>
                        <button onClick={(e) => { e.preventDefault(); dispatch(getOpen(false)) }} >x</button>
                    </div>
                    <div className='form__wrapper-button'>
                        <button type='submit' >Valider</button>
                        <button onClick={(e) => { e.preventDefault(); dispatch(getOpen(false)) }} >Annuler</button>
                    </div>
                </form>
            </div>
        </section>
    );
};

export default Modal;