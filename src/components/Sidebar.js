import React from 'react';
import { FaPowerOff } from 'react-icons/fa';
import { NavLink } from 'react-router-dom';

const Sidebar = () => {
    return (
        <nav className='sidebar'>
            <h1 className='sidebar__title'>Menu</h1>
            <ul className='sidebar__navig'>
                <li className='sidebar--navig--item'><NavLink to="/">Produits</NavLink></li>
                <li className='sidebar--navig--item'><NavLink to="/customer">Clients</NavLink></li>
                <li className='sidebar--navig--item'> <NavLink to="/orders"> Commandes</NavLink></li>
            </ul>
            <p className='sidebar__logout'><NavLink to="/"><FaPowerOff /> Deconnexion</NavLink></p>
        </nav>
    );
};

export default Sidebar;