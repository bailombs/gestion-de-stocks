import React from 'react';

const LabelInput = ({ label, labelFormat, typeInput }) => {
    return (
        <div className='form__inputwrapper'>
            <label htmlFor={labelFormat}>{label}</label>
            <input
                type={typeInput}
                id={labelFormat}

                placeholder={label}
            />
        </div>
    );
};

export default LabelInput;