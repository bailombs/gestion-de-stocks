import React, { useMemo } from 'react';
import data from '../Data/data.json';
import Card from './Card';
const AllProducts = () => {

    const DATA = useMemo(() =>
        data
        , [])
    return (
        <section className='products'>
            {DATA.map((product, index) => (
                <Card key={index} product={product} />
            ))}
        </section>
    );
};

export default AllProducts;