import React from 'react';
import { BsFillArrowLeftCircleFill } from 'react-icons/bs';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { useCurrentPage } from '../Data/useCurrentPage';
import { getOpen, getSearchContent } from '../redux/features/customersSlice';
import Modal from './Modal';

const SearchBar = () => {
    const dispatch = useDispatch()
    const isOpen = useSelector((state) => state?.customers?.open)



    return (
        <>
            <section className='search-bar'>
                <NavLink to="/" className='search-bar__back'>
                    <BsFillArrowLeftCircleFill /> Retour
                </NavLink>
                <aside className='search-bar__search'>
                    <p className='search-bar__search--text'>Recherche</p>
                    <input
                        className='search-bar__search--input'
                        type='search'
                        onChange={(e) => {
                            dispatch(getSearchContent(e.target.value))
                        }}
                        placeholder='Rechercher un produit'

                    />
                </aside>
                {
                    useCurrentPage() === 'Clients' ? (
                        <button
                            className='search-bar__addcustomer'
                            onClick={() => {
                                dispatch(getOpen(true))
                            }}
                        >
                            Ajouter un client
                        </button>
                    ) : (null)
                }

            </section>
            {isOpen && <Modal />}
        </>
    );
};

export default SearchBar;