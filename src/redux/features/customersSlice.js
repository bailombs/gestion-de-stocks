import { createSlice } from "@reduxjs/toolkit"

export const customersSlice = createSlice({
    name: 'customer',
    initialState: {
        searchContent: "",
        loading: false,
        error: null,
        open: false,
        social: "",
        address: "",
        email: "",
        tel: "",
        descritption: ""
    },
    reducers: {
        getSearchContent: (state, action) => {
            state.searchContent = action.payload
        },
        getOpen: (state, action) => {
            state.open = action.payload
        },
        getSocial: (state, action) => {
            state.social = action.payload
        },
        getAddress: (state, action) => {
            state.address = action.payload
        },
        getEmail: (state, action) => {
            state.email = action.payload
        },
        getTel: (state, action) => {
            state.tel = action.payload
        },
        getDescritption: (state, action) => {
            state.descritption = action.payload
        },
        // setCustomers: (state, action) => {
        //     state.customers = action.payload
        // },
        // addCustomer: (state, action) => {
        //     state.customers.push(action.payload)
        // },

    },
    //     extraReducers: (builder) => {

    //         builder
    //             .addCase(getCustomers.fulfilled, (state, action) => {
    //                 // Add user to the state array
    //                 state.customersItems = action.payload
    //             })
    //             .addCase(addNewCustomer.fulfilled, (state, action) => {
    //                 console.log(action.payload);
    //                 state.customersItems.push(action.payload)
    //             })
    //             .addCase(deleteCustomer.fulfilled, (state, action) => {
    //                 state.customersItems.filter((customer) => (
    //                     customer.id !== action.payload
    //                 ))
    //             })
    //         // .addCase(deleteCustomer.fulfilled, (state, action) => {
    //         //     console.log(action.payload);
    //         //     state.customersItems.filter(customer => {
    //         //         return customer.id !== action.payload
    //         //     })
    //         // })

    //         // [addNewCustomer.pending]: (state) => {
    //         //     state.isLoading = true;
    //         // },
    //         // [addNewCustomer.fulfilled]: (state, action) => {
    //         //     // console.log(action);
    //         //     state.isLoading = false;
    //         //     state.customersItems.push(action.payload)
    //         // },
    //         // [addNewCustomer.rejected]: (state, action) => {
    //         //     console.log(action);
    //         //     state.isLoading = false;
    //         // },

    //         // [getCustomers.pending]: (state) => {
    //         //     state.isLoading = true;
    //         // },
    //         // [getCustomers.fulfilled]: (state, action) => {
    //         //     // console.log(action);
    //         //     state.isLoading = false;
    //         //     state.customersItems = action.payload;
    //         // },
    //         // [getCustomers.rejected]: (state, action) => {
    //         //     console.log(action);
    //         //     state.isLoading = false;
    //         // },


    //         // [deleteCustomer.pending]: (state) => {
    //         //     state.isLoading = true;
    //         // },
    //         // [deleteCustomer.fulfilled]: (state, action) => {
    //         //     // console.log(action);
    //         //     state.isLoading = false;
    //         //     state.customersItems.filter((customer) => customer.id !== action.payload)
    //         // },

    //         // [deleteCustomer.rejected]: (state, action) => {
    //         //     console.log(action);
    //         //     state.isLoading = false;
    //         // },



    //     }
})

export const { getOpen, getSocial, getAddress, getEmail, getTel, getDescritption, setCustomers, getSearchContent } = customersSlice.actions
export default customersSlice.reducer;