import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';


export const customersApi = createApi({
    reducerPath: "customersApi",
    baseQuery: fetchBaseQuery({ baseUrl: "https://boukouty-stock.herokuapp.com/api" }),
    tagTypes: ["Customer"],
    endpoints: (builder) => ({
        customers: builder.query({
            query: () => "/clients",
            providesTags: ["Customer"]
        }),
        addNewCustomer: builder.mutation({
            query: (newCustomer) => ({
                url: "/clients",
                method: "POST",
                body: newCustomer,
            }),
            invalidatesTags: ["Customer"],
        }),
        deleteCustomer: builder.mutation({
            query: (id) => ({
                url: "/clients/" + id,
                method: "DELETE"
            }),
            invalidatesTags: ["Customer"]
        }),
        updateCustomer: builder.mutation({
            query: ({ id, ...update }) => ({
                url: `/clients/${id}`,
                method: "PUT",
                body: update,
            }),
            invalidatesTags: ["Customer"]
        }),
    }),
});

export const { useCustomersQuery, useAddNewCustomerMutation, useDeleteCustomerMutation, useUpdateCustomerMutation } = customersApi;

// export const getCustomers = createAsyncThunk(
//     'customer/getCustomers',
//     async (_, thunkAPI) => {
//         try {
//             console.log(thunkAPI);
//             console.log(thunkAPI.getState());
//             // thunkAPI.dispatch(openModal());
//             const resp = await api.get("/clients");
//             console.log(resp.data);
//             return resp.data;
//         } catch (error) {
//             return thunkAPI.rejectWithValue('something went wrong');
//         }
//     }
// );

// export const addNewCustomer = createAsyncThunk(
//     'customer/addNewCustomer',
//     async (initialPost, thunkAPI) => {
//         try {
//             // initialPost = thunkAPI.getState().customers.customersItems
//             console.log(initialPost);
//             console.log(thunkAPI.getState());

//             // thunkAPI.dispatch(openModal());
//             const resp = await api.post("/clients", initialPost);
//             console.log(resp.data);
//             return resp.data;
//         } catch (error) {
//             return thunkAPI.rejectWithValue('something went wrong');
//         }
//     }
// );

// export const deleteCustomer = createAsyncThunk(
//     'customer/deleteCustomer',
//     async (id, thunkAPI) => {
//         try {
//             // initialPost = thunkAPI.getState().customers.customersItems
//             console.log(id);
//             console.log(thunkAPI);

//             // thunkAPI.dispatch(openModal());
//             const resp = await api.delete("/clients/" + id);
//             console.log(resp.data);
//             return resp.data;
//         } catch (error) {
//             return thunkAPI.rejectWithValue('something went wrong');
//         }
//     }
// );



