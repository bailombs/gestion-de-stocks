import { configureStore } from "@reduxjs/toolkit";
import { customersApi } from './features/customersApi';
import customersReducer from './features/customersSlice';
export default configureStore({
    reducer: {
        [customersApi.reducerPath]: customersApi.reducer,
        customers: customersReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(customersApi.middleware)
});

