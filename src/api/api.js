import axios from "axios";

export default axios.create({
    baseURL: 'https://boukouty-stock.herokuapp.com/api'
});