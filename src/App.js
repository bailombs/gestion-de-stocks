import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Customer from './pages/Customer';
import Error from './pages/Error';
import Orders from './pages/Orders';
import Products from './pages/Products';
import './styles/index.scss';

const App = () => {
  return (
    <Routes>
      <Route path='/' element={<Products />} />
      <Route path='/orders' element={<Orders />} />
      <Route path='/customer' element={<Customer />} />
      <Route path='*' element={<Error />} />
    </Routes>
  );
};

export default App;
