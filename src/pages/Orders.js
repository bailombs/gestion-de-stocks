import React from 'react';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';

const Orders = () => {
    return (
        <div className='wrapper-orders'>
            <Sidebar />
            <div className='wrapper-orders__content'>
                <Header />
            </div>
        </div>
    );
};

export default Orders;