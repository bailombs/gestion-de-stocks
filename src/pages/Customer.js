import React from 'react';
import AllCustomers from '../components/AllCustomers';
import Header from '../components/Header';
import SearchBar from '../components/SearchBar';
import Sidebar from '../components/Sidebar';

const Customer = () => {
    return (
        <div className='wrapper-customers'>
            <Sidebar />
            <div className='wrapper-customers__content'>
                <Header />
                <SearchBar />
                <AllCustomers />
            </div>
        </div>
    );
};

export default Customer;