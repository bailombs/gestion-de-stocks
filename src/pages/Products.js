import React from 'react';
import AllProducts from '../components/AllProducts';
import Header from '../components/Header';
import SearchBar from '../components/SearchBar';
import Sidebar from '../components/Sidebar';


const Products = () => {
    return (
        <div className='wrapper-products'>
            <Sidebar />
            <main className='wrapper-products__content'>
                <Header />
                <SearchBar />
                <AllProducts />
            </main>
        </div>

    );
};

export default Products;